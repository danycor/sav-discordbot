/*
 * Copyright Dany CORBINEAU 2021.
 */

package sav;

import net.dv8tion.jda.api.entities.TextChannel;
import org.json.simple.parser.ParseException;
import sav.actions.LogiCode;
import net.dv8tion.jda.api.JDABuilder;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import net.dv8tion.jda.api.requests.GatewayIntent;
import org.jetbrains.annotations.NotNull;
import sav.save.Save;
import sav.save.SaveManager;

import javax.security.auth.login.LoginException;
import java.io.IOException;
import java.util.EnumSet;

public class Main extends ListenerAdapter {
    public static void main(String[] args) throws LoginException {
        Configuration.getInstance().load(args[0]);
        String token = Configuration.getInstance().getValue("DISCORD_KEY");
        EnumSet<GatewayIntent> intents = EnumSet.noneOf(GatewayIntent.class);
        intents.add(GatewayIntent.GUILD_MESSAGES);
        JDABuilder builder = JDABuilder.create(token, intents);
        builder.addEventListeners(new Main());
        builder.build();
    }

    @Override
    public void onMessageReceived(@NotNull MessageReceivedEvent event) {
        try {
            Save save = SaveManager.getInstance().loadSave(event.getGuild().getId());
            TextChannel channel = save.getTextChannelListen(event.getGuild());
            if (
                    (channel != null && event.getChannel().getName().equals(channel.getName()))
                    || event.getChannel().getName().equals("bot")
            ) {
                (new LogiCode()).run(event);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }

    }
}
