package sav;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;

public class Configuration {
    private JSONObject configuration;

    private static final Configuration instance = new Configuration();

    private Configuration() {

    }

    public static Configuration getInstance() {
        return instance;
    }

    public void load(String filePath) {
        try {
            String configFileContent = Files.readString(Path.of(filePath), StandardCharsets.UTF_8);
            JSONParser parser = new JSONParser();
            configuration = (JSONObject) parser.parse(configFileContent);
        } catch (ParseException e) {
            System.out.println("Pars config fail : " + e.getMessage());
        } catch (IOException e) {
            System.out.println("Load file config : " + e.getMessage());
        }
    }

    public String getValue(String field) {
        if (configuration != null) {
            return (String) configuration.get(field);
        }
        return "";
    }
}
