package sav.actions;

import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.TextChannel;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import org.json.simple.parser.ParseException;
import sav.model.CommandInformation;
import sav.save.Save;
import sav.save.SaveLine;
import sav.save.SaveManager;

import java.io.IOException;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class LogiCode implements Action {

    protected Save save;

    @Override
    public void run(MessageReceivedEvent event) {
        String message = event.getMessage().getContentRaw();
        try {
            save = SaveManager.getInstance().loadSave(event.getGuild().getId());
            if (isHelp(message)) {
                sendHelp(event);
            }
            if (isShowWar(message)) {
                sendTableMessage(event.getGuild(), getShowWarValue(message));
            }
            if (isSetChannels(message)) {
                setChannels(message);
                event.getChannel().sendMessage("OK").queue();
            }
            if (isAddCode(message)) {
                CommandInformation ci = getCommandInformation(message);
                addCodeToSave(ci);
                sendTableMessage(event.getGuild(), ci.getWarId());
            }
            if (isDelCode(message)) {
                CommandInformation ci = getCommandInformation(message);
                delCodeToSave(ci);
                sendTableMessage(event.getGuild(), ci.getWarId());
            }
        } catch (IllegalStateException | IOException | ParseException e) {
            event.getChannel().sendMessage("Fail to understand your command :  " + message).queue();
            sendHelp(event);
        }

    }

    protected void sendTableMessage(Guild guild, int warId) {
        TextChannel sendChannel = save.getTextChannel(guild);
        if (sendChannel != null) {
            sendChannel.sendMessage(getWarTableString(warId)).queue();
        }
    }

    protected boolean isShowWar(String command) {
        return Pattern.matches("sav-logi show war (\\d+)", command);
    }

    protected boolean isSetChannels(String command) {
        return Pattern.matches("sav-logi set channels ([\\w\\-]+) ([\\w\\-]+)", command);
    }

    protected void setChannels(String command) throws IOException {
        Pattern p = Pattern.compile("sav-logi set channels ([\\w\\-]+) ([\\w\\-]+)");
        Matcher m = p.matcher(command);
        String listenChannel = "";
        String sendChannel = "";
        while (m.find()) {
            listenChannel = m.group(1);
            sendChannel = m.group(2);
        }
        save.setSendChannel(sendChannel);
        save.setListenChannel(listenChannel);
        SaveManager.getInstance().save(save);
    }

    protected int getShowWarValue(String command) {
        Pattern p = Pattern.compile("sav-logi show war (\\d+)");
        Matcher m = p.matcher(command);
        int warId = 0;
        while (m.find()) {
            warId = Integer.parseInt(m.group(1));
        }
        return warId;
    }

    protected boolean isHelp(String command) {
        return Pattern.matches("sav-logi help", command);
    }

    protected void sendHelp(MessageReceivedEvent event) {
        event.getChannel().sendMessage(
            "\"`sav-logi show war <war number>\" - Use to show logi code for a war.`" + "\n" +
                "\"`sav-logi set channels <channel to listen> <channel to send>`\" - Use to define the listening channel (ex : bot) and the output channel (ex : code-logi)." + "\n" +
                "\"`sav-logi add code <war number> <hexagon> <city> <name> <code>`\" - Add a logi code for a war." + "\n" +
                "\"`sav-logi del code <war number> <hexagon> <city> <name> <code>`\" - Remove code for a war." + "\n"+
                "**!! Be careful with SPACES !!**"
        ).queue();
    }

    protected boolean isAddCode(String command) {
        return Pattern.matches("sav-logi add code (\\d+) ([\\w\\-]+) ([\\w\\-]+) ([\\w\\-]+) (\\d+)", command);
    }

    protected boolean isDelCode(String command) {
        return Pattern.matches("sav-logi del code (\\d+) ([\\w\\-]+) ([\\w\\-]+) ([\\w\\-]+) (\\d+)", command);
    }

    protected String buildTable(ArrayList<ArrayList<String>> table2d) {
        Map<Integer, Integer> rowSizes = new TreeMap<>();
        for (ArrayList<String> line : table2d) {
            for (String cell : line) {
                int cellKey = line.indexOf(cell);
                if (!rowSizes.containsKey(cellKey) || rowSizes.get(cellKey) < cell.length()) {
                    rowSizes.put(cellKey, cell.length());
                }
            }
        }
        StringBuilder table = new StringBuilder();
        table.append("```\n");
        table.append(buildGapRow(rowSizes));
        table.append("\n");
        for (ArrayList<String> line : table2d) {
            table.append(buildRow(rowSizes, line));
            table.append("\n");
            table.append(buildGapRow(rowSizes));
            table.append("\n");
        }
        table.append("```\n");
        return table.toString();
    }

    protected String buildGapRow(Map<Integer, Integer> rowSizes) {
        StringBuilder row = new StringBuilder("+");
        for (Map.Entry<Integer, Integer> entry : rowSizes.entrySet()) {
            row.append("-".repeat(entry.getValue()));
            row.append("+");
        }
        return row.toString();
    }

    protected String buildRow(Map<Integer, Integer> rowSizes, ArrayList<String> row) {
        StringBuilder rowString = new StringBuilder("|");
        for (String cell : row) {
            int cellKey = row.indexOf(cell);
            rowString.append(cell);
            rowString.append(" ".repeat(rowSizes.get(cellKey) - cell.length()));
            rowString.append("|");
        }
        return rowString.toString();
    }

    protected String getWarTableString(int warId) {
        List<SaveLine> saveLines = new ArrayList<>();
        for (SaveLine itSaveLine : save.getSaveLines()) {
            if (itSaveLine.getWarNumber() == warId) {
                saveLines.add(itSaveLine);
            }
        }
        return buildTable(saveListToArray(saveLines));
    }

    protected ArrayList<ArrayList<String>> saveListToArray(List<SaveLine> saveLines) {
        ArrayList<ArrayList<String>> lines = new ArrayList<>();
        ArrayList<String> header = new ArrayList<>();
        header.add("Hexagone");
        header.add("Ville");
        header.add("Nom");
        header.add("Code");
        lines.add(header);

        saveLines.sort(new Comparator<SaveLine>() {
            @Override
            public int compare(SaveLine o1, SaveLine o2) {
                if(o1.getHexagon().compareTo(o2.getHexagon()) != 0) {
                    return o1.getHexagon().compareTo(o2.getHexagon());
                }
                if(o1.getCityName().compareTo(o2.getCityName()) != 0) {
                    return o1.getCityName().compareTo(o2.getCityName());
                }
                if(o1.getStockpileName().compareTo(o2.getStockpileName()) != 0) {
                    return o1.getStockpileName().compareTo(o2.getStockpileName());
                }
                if(o1.getStockpileCode().compareTo(o2.getStockpileCode()) != 0) {
                    return o1.getStockpileCode().compareTo(o2.getStockpileCode());
                }
                return 0;
            }
        });
        for (SaveLine saveLine : saveLines) {
            ArrayList<String> row = new ArrayList<>();
            row.add(saveLine.getHexagon().toUpperCase());
            row.add(saveLine.getCityName().toUpperCase());
            row.add(saveLine.getStockpileName().toUpperCase());
            row.add(saveLine.getStockpileCode());
            lines.add(row);
        }
        return lines;
    }


    protected void addCodeToSave(CommandInformation ci) throws IOException {
        boolean codeAlreadyExist = false;
        for (SaveLine saveLine : save.getSaveLines()) {
            if (
                    saveLine.getWarNumber() == ci.getWarId()
                            && saveLine.getHexagon().equals(ci.getHexagon())
                            && saveLine.getCityName().equals(ci.getCityName())
                            && saveLine.getStockpileName().equals(ci.getStockpileName())
                            && saveLine.getStockpileCode().equals(String.valueOf(ci.getStockpileCode()))
            ) {
                codeAlreadyExist = true;
                break;
            }
        }
        if (!codeAlreadyExist) {
            SaveLine saveLine = new SaveLine();
            saveLine.setWarNumber(ci.getWarId());
            saveLine.setHexagon(ci.getHexagon());
            saveLine.setCityName(ci.getCityName());
            saveLine.setStockpileName(ci.getStockpileName());
            saveLine.setStockpileCode(String.valueOf(ci.getStockpileCode()));
            save.getSaveLines().add(saveLine);
            SaveManager.getInstance().save(save);
        }

    }

    protected void delCodeToSave(CommandInformation ci) throws IOException {
        List<SaveLine> linesToKeep = new ArrayList<>();
        for (SaveLine saveLine : save.getSaveLines()) {
            if (
                    saveLine.getWarNumber() != ci.getWarId()
                            || !saveLine.getHexagon().equals(ci.getHexagon())
                            || !saveLine.getCityName().equals(ci.getCityName())
                            || !saveLine.getStockpileName().equals(ci.getStockpileName())
                            || !saveLine.getStockpileCode().equals(String.valueOf(ci.getStockpileCode()))
            ) {
                linesToKeep.add(saveLine);
            }
        }
        save.setSaveLines(linesToKeep);
        SaveManager.getInstance().save(save);
    }

    protected CommandInformation getCommandInformation(String message) {
        Pattern p = Pattern.compile("sav-logi ([\\w\\-]+) code (\\d+) ([\\w\\-]+) ([\\w\\-]+) ([\\w\\-]+) (\\d+)");
        Matcher m = p.matcher(message);
        while (m.find()) {
            CommandInformation ci = new CommandInformation();
            ci.setWarId(Integer.parseInt(m.group(2)));
            ci.setHexagon(m.group(3));
            ci.setCityName(m.group(4));
            ci.setStockpileName(m.group(5));
            ci.setStockpileCode(Integer.parseInt(m.group(6)));
            return ci;
        }
        return new CommandInformation();
    }
}
