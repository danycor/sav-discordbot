package sav.save;

import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.TextChannel;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

public class Save {
    private Path filePath;

    private List<SaveLine> saveLines = new ArrayList<>();

    private String listenChannel = "bot";
    private String sendChannel = "bot";

    public List<SaveLine> getSaveLines() {
        return saveLines;
    }

    public void setSaveLines(List<SaveLine> saveLines) {
        this.saveLines = saveLines;
    }

    public Path getFilePath() {
        return filePath;
    }

    public void setFilePath(Path filePath) {
        this.filePath = filePath;
    }

    public String getListenChannel() {
        return listenChannel;
    }

    public void setListenChannel(String listenChannel) {
        this.listenChannel = listenChannel;
    }

    public String getSendChannel() {
        return sendChannel;
    }

    public TextChannel getTextChannel(Guild guild) {
        for (TextChannel tChannel : guild.getTextChannels()) {
            if (tChannel.getName().equals(sendChannel)) {
                return tChannel;
            }
        }
        return null;
    }

    public TextChannel getTextChannelListen(Guild guild) {
        for (TextChannel tChannel : guild.getTextChannels()) {
            if (tChannel.getName().equals(listenChannel)) {
                return tChannel;
            }
        }
        return null;
    }

    public void setSendChannel(String sendChannel) {
        this.sendChannel = sendChannel;
    }
}
