package sav.save;

public class SaveLine {
    private String cityName;
    private String hexagon;
    private String stockpileName;
    private String stockpileCode;
    private int warNumber;

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName.toUpperCase();
    }

    public String getHexagon() {
        return hexagon;
    }

    public void setHexagon(String hexagon) {
        this.hexagon = hexagon.toUpperCase();
    }

    public String getStockpileName() {
        return stockpileName;
    }

    public void setStockpileName(String stockpileName) {
        this.stockpileName = stockpileName.toUpperCase();
    }

    public String getStockpileCode() {
        return String.format("%6s", stockpileCode).replace(' ', '0');
    }

    public void setStockpileCode(String stockpileCode) {
        this.stockpileCode = stockpileCode.toUpperCase();
    }

    public int getWarNumber() {
        return warNumber;
    }

    public void setWarNumber(int warNumber) {
        this.warNumber = warNumber;
    }
}
