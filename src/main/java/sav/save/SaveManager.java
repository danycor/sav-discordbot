package sav.save;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import sav.Configuration;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

public class SaveManager {

    private static final SaveManager instance = new SaveManager();

    private SaveManager() {

    }

    public static SaveManager getInstance() {
        return instance;
    }

    protected void createSaveFileIfNotExist(Path filePath)
    {
        try {
            if (!filePath.toFile().exists()) {
                filePath.toFile().getParentFile().mkdir();
                filePath.toFile().createNewFile();
                Files.writeString(filePath, getDefaultSaveJson().toJSONString());
            }
        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    protected JSONObject getDefaultSaveJson() {
        Save save = new Save();
        JSONObject saveJson = new JSONObject();
        JSONArray saveLinesJson = new JSONArray();
        saveJson.put("lines", saveLinesJson);
        saveJson.put("listen", save.getListenChannel());
        saveJson.put("send", save.getSendChannel());
        return saveJson;
    }

    public Save loadSave(String guildId) throws IOException, ParseException {
        String saveFilePath = this.getSaveFilePath(guildId);
        Path filePath = Path.of(saveFilePath);
        this.createSaveFileIfNotExist(filePath);

        String save_content = Files.readString(filePath, StandardCharsets.UTF_8);
        JSONParser parser = new JSONParser();

        JSONObject saveJson = (JSONObject) parser.parse(save_content);
        Save save = new Save();
        save.setFilePath(filePath);
        save.setListenChannel(saveJson.get("listen").toString());
        save.setSendChannel(saveJson.get("send").toString());

        List<SaveLine> saveLines = new ArrayList<>();
        for (var it = ((JSONArray)saveJson.get("lines")).iterator(); it.hasNext(); ) {
            JSONObject lineJson = (JSONObject) it.next();
            SaveLine line = new SaveLine();
            line.setWarNumber(Integer.parseInt(lineJson.get("war-number").toString()));
            line.setCityName(lineJson.get("city-name").toString());
            line.setHexagon(lineJson.get("hexagon").toString());
            line.setStockpileName(lineJson.get("stockpile-name").toString());
            line.setStockpileCode(lineJson.get("stockpile-code").toString());
            saveLines.add(line);
        }
        save.setSaveLines(saveLines);
        return save;
    }

    public void save(Save save) throws IOException {

        JSONObject saveJson = new JSONObject();


        JSONArray saveLinesJson = new JSONArray();
        saveJson.put("lines", saveLinesJson);
        saveJson.put("listen", save.getListenChannel());
        saveJson.put("send", save.getSendChannel());

        for (SaveLine line: save.getSaveLines()) {
            JSONObject lineJson = new JSONObject();
            saveLinesJson.add(lineJson);
            lineJson.put("war-number", line.getWarNumber());
            lineJson.put("city-name", line.getCityName());
            lineJson.put("hexagon", line.getHexagon());
            lineJson.put("stockpile-name", line.getStockpileName());
            lineJson.put("stockpile-code", line.getStockpileCode());

        }
        Files.writeString(save.getFilePath(), saveJson.toJSONString());
    }

    protected String getSaveFilePath(String guildId) {
        return Configuration.getInstance().getValue("SAVE_DIR_PATH") + "/" + guildId + ".json";
    }
}
