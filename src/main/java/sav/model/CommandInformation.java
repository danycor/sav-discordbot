package sav.model;

public class CommandInformation {
    private int warId;
    private String hexagon;
    private String cityName;
    private String stockpileName;
    private int stockpileCode;

    public int getWarId() {
        return warId;
    }

    public void setWarId(int warId) {
        this.warId = warId;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getHexagon() {
        return hexagon;
    }

    public void setHexagon(String hexagon) {
        this.hexagon = hexagon;
    }

    public String getStockpileName() {
        return stockpileName;
    }

    public void setStockpileName(String stockpileName) {
        this.stockpileName = stockpileName;
    }

    public int getStockpileCode() {
        return stockpileCode;
    }

    public void setStockpileCode(int stockpileCode) {
        this.stockpileCode = stockpileCode;
    }
}
