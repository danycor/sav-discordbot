FROM openjdk:16
COPY ./SAV-LOGI-1.1-all.jar /app/SAV-logi
WORKDIR /app
ENTRYPOINT ["java", "-jar", "SAV-logi", "configuration.json"]
